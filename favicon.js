"use strict";
var middleware = require("serve-favicon");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:favicon");
  debug("start");

  debug(".useSetup");
  imports.express.useSetup(middleware(options.iconPath));

  debug("register nothing");
  register(null, {});
};
